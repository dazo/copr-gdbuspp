#%%define releasetag .qa1
#%%define versiontag _qa1

%global _hardened_build 1
%global _vpath_srcdir %{name}-%{version}%{?versiontag}

Name:     gdbuspp
Version:  3
Release:  1%{?releasetag}%{?dist}
Summary:  GDBus++ - glib2 D-Bus wrapper for C++
License:  AGPLv3
URL:      https://codeberg.org/OpenVPN/gdbuspp/
Source0:  https://swupdate.openvpn.net/community/releases/%{name}-%{version}%{?versiontag}.tar.xz
Source1:  https://swupdate.openvpn.net/community/releases/%{name}-%{version}%{?versiontag}.tar.xz.asc
Source2:  gpgkey-F554A3687412CFFEBDEFE0A312F5F7B42F2B01E7.gpg
Vendor:   OpenVPN Inc

ExcludeArch: arm7hl i686

BuildRequires: gcc-c++
BuildRequires: gnupg2
BuildRequires: python3-dbus
BuildRequires: python3-gobject-base
BuildRequires: meson
BuildRequires: glib2-devel
BuildRequires: dbus-devel
BuildRequires: python3-xmltodict

%description
GDBus++ is an API wrapper around glib2's D-Bus interface to write D-Bus
services and proxies (clients) using C++17.  It tries to avoid exposing
the low-level glib2 APIs as much as possible to the developer.

%package devel
Summary:   Development headers for GDBus++
Requires:  %{name} = %{version}

%description devel
Contains all the development headers needed to compile an application
implementing the GDBus++ API

%prep
gpgv2 --quiet --keyring %{SOURCE2} %{SOURCE1} %{SOURCE0}
%autosetup -c

%build
%meson
%meson_build

%check
# Only stand-alone tests are enabled, as other tests depends
# on a dbus daemon/broker being available and functional.  This
# is not the case in Koji/Copr builders
%meson_test --suite standalone

%install
%meson_install

%files
%{_libdir}/lib%{name}.so.*
%{_pkgdocdir}/README.md

%files devel
%{_libdir}/lib%{name}.a
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/%{name}
%{_pkgdocdir}/dbus-primer.md
%{_pkgdocdir}/example-*.cpp


%changelog
* Tue Dec  3 2024 David Sommerseth <davids@openvpn.net> - 3-1
- Updated to GDBus++ v3 release

* Wed Aug 28 2024 David Sommerseth <davids@openvpn.net> - 2-1
- Updated to latest upstream release
- Relocated development related docs to gdbuspp-devel

* Mon Jun 17 2024 David Sommerseth <davids@openvpn.net> - 1-1
- First official release of GDBus++
